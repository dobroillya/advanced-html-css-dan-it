// init modules
const {src, dest, watch, series, parallel } = require('gulp');
const autoprefixer = require ('autoprefixer');
const cssnano= require ('cssnano');
const concat = require ('gulp-concat');
const postcss = require ('gulp-postcss');
const imagemin = require('gulp-imagemin');
const sass = require('gulp-sass')(require('sass'));
const sourcesmaps = require ('gulp-sourcemaps');
const uglify = require ('gulp-uglify');
const clean = require('gulp-clean');

//file path variables
const files = {
    scssPath: 'src/scss/**/*.scss',
    jsPath: 'src/js/**/*.js'
}

//Sass task
function cleanDist() {
    return src('dist', { allowEmpty: true, read: false })
        .pipe(clean());
}


function scssTask(){
    return src(files.scssPath)
        .pipe(concat('styles.min.css'))
        .pipe(sourcesmaps .init())
        .pipe(sass())
        .pipe(postcss([autoprefixer(), cssnano()]))
        .pipe(sourcesmaps .write('.'))
        .pipe(dest('dist'))

}

//JS task

function jsTask(){
    return src(files.jsPath)
    .pipe(concat('scripts.min.js'))
    .pipe(uglify())
    .pipe(dest('dist'))
}

//img
function optimizeImages() {
    return src('src/img/**/*.{png,jpg,jpeg}')
      .pipe(imagemin())
      .pipe(dest('dist/images'));
  }



// Watch task
function watchTask(){
    watch([files.scssPath, files.jsPath],
        parallel(scssTask, jsTask))
}


//Default task

exports.build = series(
    cleanDist,
    parallel(scssTask, jsTask, optimizeImages),
    watchTask
);

exports.dev = series(
    parallel(scssTask, jsTask),
    watchTask
);
