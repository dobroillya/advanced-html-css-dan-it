const mobilMenu = document.querySelector(".header__mobil-menu");
const openBtn = document.querySelector("[data-open]");
const closeBtn = document.querySelector("[data-close]");
const navBar = document.querySelector(".navbar");

handleOpen = () => {
  openBtn.classList.toggle("no-show-mobil-btn");
  closeBtn.classList.toggle("no-show-mobil-btn");
  navBar.classList.toggle("no-show-mobil-btn");
};

mobilMenu.addEventListener("click", handleOpen);
